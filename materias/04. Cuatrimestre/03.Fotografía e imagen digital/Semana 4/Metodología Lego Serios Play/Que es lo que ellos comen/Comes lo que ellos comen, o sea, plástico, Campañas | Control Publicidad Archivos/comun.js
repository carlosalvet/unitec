var ver_pop_suscripcion = 0;

$(document).ready(function () {

    $(document).on( {click: function () {
			var id_noticia = $(this).attr('href');
			ver_video_preroll(id_noticia);
			return false;
		}
	}, ".bt_video");


	$('.close').on('click', function()		{
		var video = document.getElementById("bgvid");
		video.pause();
		$("#bgvid").attr("src", "");
		video.currentTime = 0;
		$('html').css('overflowY', 'scroll'); 
		$(".pop_vid").hide();
	});	

	$('.btn-suscribete').on('click', function(){
		$(".bots").find(".btn").removeClass("active");
		$(this).addClass("active");
	});



    var k = 'suscriptor', v = 'true';
	if (readCookie(k) !== v) {     // Si no existe la cookie mostramos pop
		ver_pop_suscripcion = 1;
	}

	$('.btn-close').on('click', function(){
		ver_pop_suscripcion = 0;
		$('.pop-suscripcion').hide();
		return false;
	});



	$(".dropdown-submenu").hover(
		function(){ $(this).addClass('open') },
		function(){ $(this).removeClass('open') }
	);

	$('.go_scrollTo').on('click', function(){
		var scroll_a = $(this).attr('data-target');
		$.scrollTo(scroll_a, 500, {axis:'y'} );
	});

	$('.share').on('click', function(){	
		url = $(this).attr("href");
		window.open(url,'ventanacompartir', 'toolbar=0, status=0, width=650, height=450');
		return false;
	}); 

	$("#bt_buscar").click(function()			{	f_buscar(); });
	$("#bt_suscripcion").click(function()		{	f_suscripcion_newsletter(); });
	$("#bt_enviar_contacto").click(function()	{	f_enviar_contacto(); });

	$(".btn-responder").click(function()	{
		hastag = "@" + $(this).attr("data-hastag");
		cita = $(this).attr("data-cita");
		$("#debates_opinion").val(hastag);
		$("#debates_cita").val(cita);
		$("#debates_nombre").focus(); $.scrollTo("100", 500, {axis:'y'} ); 
	});

	if ( $(".fancybox-media").length > 0 ) {
		$('.fancybox-media').fancybox({
			'padding'       : 0,
			'titleShow'     : false,
			'width'			: '854px',
			'height'		: '480px',
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic',
			'easingIn'      : 'easeOutBack',
			'easingOut'     : 'easeInBack',

			'type'			: 'iframe'

		});
	}

	if ( $(".fancybox").length > 0 ) {
		$(".fancybox").fancybox({
			'padding'       : 0,
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic',
			'easingIn'      : 'easeOutBack',
			'easingOut'     : 'easeInBack',
		});
	}


	function set_top_img()
	{
		var window_height = $(window).scrollTop();
		if ($('.img_n1 img').length){

			$(".img_n1").each(function (index) 
			{ 
				var w = $(this).width();
				var top = $(this).find("img").attr( "data-top" );

				if (w==612)	{ top = parseInt(top) + 90;		}
				if (w==465)	{ top = parseInt(top) + 100;	}

				$(this).find("img").css ({  'top': top+'px'});
			})

		}

		if ($('.img_n2 img').length){

			$(".img_n2").each(function (index) 
			{ 
				var w = $(this).width();
				var top = $(this).find("img").attr( "data-top" );
				top = parseInt(top) / 3;
				if (w==210)	{ top = 0;		}

				$(this).find("img").css ({  'top': top+'px'});
			})

		}

		if ($('.img_n4 img').length){

			$(".img_n4").each(function (index) 
			{ 
				var w = $(this).width();
				var top = $(this).find("img").attr( "data-top" );
				top = parseInt(top) / 4;
				if (w==210)	{ top = 0;		}

				$(this).find("img").css ({  'top': top+'px'});
			})

		}

	}

	$(window).resize(function() {
		set_top_img();
	});

	set_top_img();

});


var data_video = ""; 
var video_id_noticia = "";
var page_name = "";
function ver_video_preroll (id_noticia) {

	url = dominio + 'includes/ajax_info_video.php?id_noticia='+id_noticia;
	video_id_noticia = id_noticia;
	$.ajax({
	  url: url, 
	  error: function (jqXHR, exception) {
		var res = jqXHR.responseText.split(";");
		imagen = res[0];
		video_preroll = res[1];
		data_video = res[2];
		id_ad = res[3];
		page_name = res[4];

		if (video_preroll=="")
		{
			$(".saltar_anuncio").hide();
			$(".timer_saltar_anuncio").hide();
			$(".link_preroll").hide();
			ver_video();
		} else {
			$(".link_preroll").show();
			$(".timer_saltar_anuncio").show();
			ver_preroll(video_preroll, imagen, id_ad, page_name);
		}

		$('.link_preroll').on('click', function(){	
			url = dominio + "ck_patrocinador.php?id=" + id_ad + "&page_name=" + page_name;
			var myWindow = window.open(url);
			return false;
		}); 
		$('.saltar_anuncio').on('click', function(){	
			$(".timer_saltar_anuncio").hide();
			$(".saltar_anuncio").hide();
			var video = document.getElementById("bgvid");
			video.pause();
			video.removeEventListener("timeupdate", currentTime); 
			video.removeEventListener("ended", fin_video); 
			ver_video(id_noticia);
			return false;
		}); 
      },

	  success: function(data) {
		  		
		var res = data.split(";");
		imagen = res[0];
		video_preroll = res[1];
		data_video = res[2];
		id_ad = res[3];
		page_name = res[4];

		if (video_preroll=="")
		{
			$(".saltar_anuncio").hide();
			$(".timer_saltar_anuncio").hide();
			$(".link_preroll").hide();
			ver_video();
		} else {
			$(".link_preroll").show();
			$(".timer_saltar_anuncio").show();
			ver_preroll(video_preroll, imagen, id_ad, page_name);
		}

		$('.link_preroll').on('click', function(){	
			url = dominio + "ck_patrocinador.php?id=" + id_ad + "&page_name=" + page_name;
			var myWindow = window.open(url);
			return false;
		}); 
		$('.saltar_anuncio').on('click', function(){	
			$(".timer_saltar_anuncio").hide();
			$(".saltar_anuncio").hide();
			var video = document.getElementById("bgvid");
			video.pause();
			video.removeEventListener("timeupdate", currentTime); 
			video.removeEventListener("ended", fin_video); 
			ver_video();
			return false;
		}); 


	  }
	});

}

function ver_preroll(video_preroll, imagen, id_ad, page_name) {  
	$.ajax({
	  //url: dominio + 'includes/ajax_video_preroll.php?id_noticia='+ id_noticia, 
	  url: dominio + 'includes/0_ajax_ad_sana_envidia.php?tipo=1&id='+ id_ad + "&page_name=" + page_name, 
	  success: function(data) {
		//alert(data);
	  }
	});

	$(".pop_vid").show();
	var src = dominio + "uploads/sana_envidia/" + video_preroll;
	var src_imagen = dominio + "uploads/sana_envidia/" + imagen;

	var video = document.getElementById("bgvid");
	$("#bgvid").attr("src", src);
	video.currentTime = 0;
	$("#next").attr("src", src_imagen);

	if (video.canPlayType) {   
		var playMsg = video.canPlayType('video/mp4; codecs="avc1.42E01E"'); //"mp4/H.264
		if (playMsg=="") { playMsg = video.canPlayType('video/ogg; codecs="theora"'); }
		$('html').css('overflowY', 'hidden'); 
		video.load();  // if HTML source element is used 
		video.play();

		video.addEventListener("timeupdate", currentTime);
		video.addEventListener("ended", fin_video);

	} 
}

function fin_video(){
	$(".timer_saltar_anuncio").hide();
	$(".saltar_anuncio").hide();
	$(".link_preroll").hide();
	var video = document.getElementById("bgvid");
	video.removeEventListener("timeupdate", currentTime); 
	video.removeEventListener("ended", fin_video); 
	$.ajax({
	  url: dominio + 'includes/0_ajax_ad_sana_envidia.php?tipo=2&id='+ id_ad + "&page_name=" + page_name
	});
	ver_video();
}

function ver_video(){
	$.ajax({
	  url: dominio + 'includes/ajax_video.php?id='+ video_id_noticia
	});
	$(".link_preroll").hide();
	$(".pop_vid").show();
	var src = dominio + "uploads/sana_envidia/" + data_video;
	var video = document.getElementById("bgvid");
	$("#bgvid").attr("src", src);
	video.currentTime = 0;
	if (video.canPlayType) {   
		var playMsg = video.canPlayType('video/mp4; codecs="avc1.42E01E"'); //"mp4/H.264
		if (playMsg=="") { playMsg = video.canPlayType('video/ogg; codecs="theora"'); }
		$('html').css('overflowY', 'hidden'); 
		video.load();  // if HTML source element is used 
		video.play();
	} 
}


function currentTime() {
	var tiempo = 5;
	var video = document.getElementById("bgvid");
	t =  hora(video.currentTime);

	tiempo = tiempo - t;
	if (tiempo == 0)
	{
		$(".timer_saltar_anuncio").hide();
		$(".saltar_anuncio").show();		
	} else {
	    document.getElementById("tiemposegundos").innerHTML = tiempo;
	}
}
function hora(segundos){
      var d=new Date(segundos*1000); 
      // Ajuste de las 23 horas
      var hora = (d.getHours()==0)?23:d.getHours()-1;
      var hora = (hora<9)?"0"+hora:hora;
      var minuto = (d.getMinutes()<9)?"0"+d.getMinutes():d.getMinutes();
      var segundo = (d.getSeconds()<9)?"0"+d.getSeconds():d.getSeconds();
      //return hora+":"+minuto+":"+segundo;	 
	  return d.getSeconds();
}

$(window).scroll(function() {
	var px_cab = $("header").height() - $(".navbar-default").height();
	var h = $("header").height();
    var height = $(window).scrollTop();

	if ($("header").hasClass('top')){
		if(height < px_cab) {
			$("header").removeClass("top").css({  'top':  '0px' });
			$('#sep_top').css({  'height': '0px' });
			$('#sep_top').css( { "background-position": "center -100px;" });
		}
    }else{
		if(height > px_cab) {
			$("header").addClass("top").css({  'top':  '-' +px_cab+'px' });
			$('#sep_top').css({  'height':  h+'px' });		

			if (ver_pop_suscripcion == 1)
			{
				$(".pop-suscripcion").show('slow'); 
			}
		}
    }


    if(height > px_cab) {
		$("header").addClass("top");
		$('#sep_top').css({  'height':  h+'px' });
        $('body').css('background-position-y', "-340px");

    } else { 
		//$("header").removeClass("top");
		$('#sep_top').css({  'height': '0px' });
		if( height > 0) {
			$('body').css('background-position-y', "-" + height +'px');
		} else  {
			$('body').css('background-position-y', "top");
		}
	}



	$(".cuerpo_noticia").find("p").first().is(function() {
		var viwewing =  $(this).visible();

		if(viwewing){ 			

			if (! $(this).hasClass('viwewing')){
				$(this).addClass( "viwewing" );

				id_noticia = $(".cuerpo_noticia").attr('data-id');
				url = dominio + 'includes/0_ajax_ad_content.php?id='+ id_noticia;
				$.ajax({
				  url: url,					 
				  success: function(data) {
				
					if (data!="")
					{
						$(".cuerpo_noticia").find("p").first().append( "<div class='ad'></div>" );
						$(".ad").html(data);
						/*
						$(".ad").animate({
							height: "375px"
						  }, 1500, function() {				

						});
						*/

						$('.close_ad_content').on('click', function()		{
							$(".ad").animate({
								height: "0px"
							  }, 1500, function() {				
								$(".ad").html("");
								$(".ad").remove();
							});	
							return false;
						});	
					}
				  }
				});


				

			}

		}
	});  





});

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}


function getCheckedRadioValue(radioGroupName) {
   var rads = document.getElementsByName(radioGroupName),
    i;
   for (i=0; i < rads.length; i++)
      if (rads[i].checked)
          return rads[i].value;
   return 0; // or undefined, or your preferred default for none checked
}

function f_confirmar_baja() {
	var ok = true;

	motivo_select = getCheckedRadioValue("motivo");

	if (motivo_select == "0")
	{
		ok=false;
		alert("Indica el un motivo de tu cancelación");

	}

	if (motivo_select == "Otros motivos")
	{
		otros_motivos = no_XSS(document.getElementById("otros_motivos").value);
		document.getElementById("otros_motivos").value = otros_motivos;
		if ((otros_motivos=="") && ok!=false){
			ok=false;
			alert("Indica el motivo de la cancelación");
			document.getElementById("otros_motivos").focus();
		}
	}
	if (ok){	
		document.getElementById("action").value = "baja_confirmada";
		document.form_baja.submit();
	}

}

function f_buscar() {
	var buscar = no_XSS(document.getElementById("q").value);
	document.getElementById("q").value = buscar;
	document.form_buscardor.submit();
}

function validar(e) {
	if (window.event) {keyval=e.keyCode}
	else
		if (e.which) {keyval=e.which}
		if (keyval=="13" && document.getElementById("q").value!="") { f_buscar(); }
}

function f_fancybox_close() {
	$.fancybox.close();
	$('.tapa').hide();
}



function f_ad_contador_impresiones(a,b,c) {
	$.ajax({
	  url: dominio + 'includes/ajax_ad_server.php?a='+ a + '&b='+b + '&c='+c, 
	  success: function(data) {
		//alert(data);
	  }
	});
}



function f_suscripcion_newsletter()
{
	var ok = true;

	suscripcion_nombre = no_XSS(document.getElementById("suscripcion_nombre").value);
	document.getElementById("suscripcion_nombre").value = suscripcion_nombre;
	if ((suscripcion_nombre=="" || suscripcion_nombre=="Nombre") && ok!=false){
		ok=false;
		$("#tx_error_suscripcion").html("Tu nombre es obligatorio");
		document.getElementById("suscripcion_nombre").focus();
	}

	var suscripcion_email = no_XSS(document.getElementById("suscripcion_email").value);
	document.getElementById("suscripcion_email").value = suscripcion_email;
	if ((suscripcion_email=="" || suscripcion_email=="e-mail") && ok!=false){
		ok=false;
		$("#tx_error_suscripcion").html("El e-mail es obligatorio"); 
		document.getElementById("suscripcion_email").focus();
	} else {
		var valor = validarEmail(suscripcion_email);
		if (!valor && ok!=false){
			ok=false;		
			$("#tx_error_suscripcion").html("El e-mail no es correcto");
			document.getElementById("suscripcion_email").value="";
			document.getElementById("suscripcion_email").focus();
		} 
	}

	valor = $("#acepto_politica").is(':checked');
	if ( !valor && ok!=false){
		ok=false;
		$("#tx_error_suscripcion").html("Tienes aceptar el Aviso Legal y Política de Privacidad");
		document.getElementById("acepto_politica").focus();
	}

	if (ok){	
		$('.tapa').show();
		document.form_suscripcion.submit();
	}
}


function f_enviar_opinion()
{
	var ok = true;

	debates_nombre = no_XSS(document.getElementById("debates_nombre").value);
	document.getElementById("debates_nombre").value = debates_nombre;
	if ((debates_nombre=="" || debates_nombre=="Tu nombre") && ok!=false){
		ok=false;
		$("#tx_error_participa").html("Tu nombre es obligatorio");
		document.getElementById("debates_nombre").focus();
	}

	var debates_email = no_XSS(document.getElementById("debates_email").value);
	document.getElementById("debates_email").value = debates_email;
	if ((debates_email=="" || debates_email=="Tu email") && ok!=false){
		ok=false;
		$("#tx_error_participa").html("El email es obligatorio"); 
		document.getElementById("debates_email").focus();
	} else {
		var valor = validarEmail(debates_email);
		if (!valor && ok!=false){
			ok=false;		
			$("#tx_error_participa").html("El email no es correcto");
			document.getElementById("debates_email").value="";
			document.getElementById("debates_email").focus();
		} 
	}

	debates_opinion = no_XSS(document.getElementById("debates_opinion").value);
	document.getElementById("debates_opinion").value = debates_opinion;
	if ((debates_opinion=="" || debates_opinion=="Tu opinión") && ok!=false){
		ok=false;
		$("#tx_error_participa").html("Tu opinión es obligatoria");
		document.getElementById("debates_opinion").focus();
	}

	valor = $("#acepto_politica").is(':checked');
	if ( !valor && ok!=false){
		ok=false;
		$("#tx_error_participa").html("Tienes aceptar el Aviso Legal y Política de Privacidad");
		document.getElementById("acepto_politica").focus();
	}

	if (ok){	
		$('.tapa').show();
		document.form_participa.submit();
	}
}

function f_enviar_contacto()
{
	var ok = true;

	contacto_nombre = no_XSS(document.getElementById("contacto_nombre").value);
	document.getElementById("contacto_nombre").value = contacto_nombre;
	if ((contacto_nombre=="" || contacto_nombre=="Nombre") && ok!=false){
		ok=false;
		$("#tx_error_contacto").html("Tu nombre es obligatorio");
		document.getElementById("contacto_nombre").focus();
	}

	var contacto_email = no_XSS(document.getElementById("contacto_email").value);
	document.getElementById("contacto_email").value = contacto_email;
	if ((contacto_email=="" || contacto_email=="eMail") && ok!=false){
		ok=false;
		$("#tx_error_contacto").html("El e-mail es obligatorio"); 
		document.getElementById("contacto_email").focus();
	} else {
		var valor = validarEmail(contacto_email);
		if (!valor && ok!=false){
			ok=false;		
			$("#tx_error_contacto").html("El e-mail no es correcto");
			document.getElementById("contacto_email").value="";
			document.getElementById("contacto_email").focus();
		} 
	}

	val = no_XSS(document.getElementById("contacto_telefono").value);
	document.getElementById("contacto_telefono").value = val;

	contacto_mensaje = no_XSS(document.getElementById("contacto_mensaje").value);
	document.getElementById("contacto_mensaje").value = contacto_mensaje;


	valor = $("#acepto_politica").is(':checked');
	if ( !valor && ok!=false){
		ok=false;
		$("#tx_error_contacto").html("Tienes aceptar el Aviso Legal y Política de Privacidad");
		document.getElementById("acepto_politica").focus();
	}

	if (ok){	
		$('.tapa').show();
		document.form_contacto.submit();
	}
}



function checkvalidate(checks) {
	 var selected = ''; 
	$(checks +' input[type=checkbox]').each(function(){
		if (this.checked) {
			selected += $(this).val()+', ';
		}
	}); 

    if (selected != '') 
		return true;
	else
	    return false;

}

function no_XSS (valor) {
	valor = replace(valor,"javascript","")
	valor = replace(valor,"<","")
	valor = replace(valor,">","")
	valor = replace(valor,"alert","")
	valor = replace(valor,"\"","")
	valor = replace(valor,"+","")
	valor = trim(valor)
	return valor;
}
// Removes leading whitespaces
function LTrim( value ) {	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");	
}

// Removes ending whitespaces
function RTrim( value ) {	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");	
}

// Removes leading and ending whitespaces
function trim( value ) {	
	return LTrim(RTrim(value));	
}
function replace(texto,s1,s2){
	return texto.split(s1).join(s2);
}

function validarEmail(valor) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor))
	{
		return (true)
	} else {
		return (false);
	}
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function solo_numeros(e) { // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla==8) return true; // 3
	patron = /\d/; // Solo acepta números
    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
} 
function solo_letras(e) { // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla==8) return true; // 3
	patron =/[A-Za-zñÑ\s]/; // Solo acepta letras, pero acepta también las letras ñ y Ñ
    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
} 
function validarFecha(Cadena){   
    var Fecha= new String(Cadena);  
    var RealFecha= new Date();  
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("/")+1,Fecha.length))   
    var Mes= new String(Fecha.substring(Fecha.indexOf("/")+1,Fecha.lastIndexOf("/")))   
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("/")))   
  
    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){   
        //alert('El formato de Año es incorrecto')   
        return false   
    }   
    // Valido el Mes   
    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){   
        //alert('El formato de Mes es incorrecto')   
        return false   
    }   
    // Valido el Dia   
    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){   
       	//alert('El formato de Día es incorrecto')   
        return false   
    }   
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {   
        if (Mes==2 && Dia > 28 || Dia>30) {   
           //alert('Día incorrecto')   
            return false   
        }   
    }   
	return (true)
} 

ven=""
function ventana(url,alto,ancho,titulo){		
	topV=(screen.availHeight/2) - alto/2;
	leftV=(screen.availWidth/2) - ancho/2;
	win="fullscreen=0,directories=0,resizable=0,location=0,status=1,scrollbars=1,toolbar=0,menubar=0,width="+ancho+",height="+alto+",screenX="+leftV+",screenY="+topV+",top="+topV+",left="+leftV
	ven=window.open(url,'ven2',win);
	window.ven.focus();
}

function unescape_html(cadena)
{	
	cadena=cadena.replace("&lt;", "<")
	cadena=cadena.replace("&gt;", ">")
	cadena=cadena.replace("&amp;", "&")
	cadena=cadena.replace("&quot;", "'")

	cadena=cadena.replace("&aacute;","á")
	cadena=cadena.replace("&eacute;","é")
	cadena=cadena.replace("&iacute;","í")
	cadena=cadena.replace("&oacute;","ó")
	cadena=cadena.replace("&uacute;","ú")

	cadena=cadena.replace("&Aacute;","Á")
	cadena=cadena.replace("&Eacute;","É")
	cadena=cadena.replace("&Iacute;","Í")
	cadena=cadena.replace("&Oacute;","Ó")
	cadena=cadena.replace("&Uacute;","Ú")

	cadena=cadena.replace("&iquest;", "¿")
	cadena=cadena.replace("&ntilde;", "ñ")	
	cadena=cadena.replace("&nbsp;", " ")
	return cadena;
}


var numeros="0123456789";
var letras="abcdefghyjklmnñopqrstuvwxyz";
var letras_mayusculas="ABCDEFGHYJKLMNÑOPQRSTUVWXYZ";

function tiene_numeros(texto){
   for(i=0; i<texto.length; i++){
      if (numeros.indexOf(texto.charAt(i),0)!=-1){
         return 1;
      }
   }
   return 0;
} 

function tiene_letras(texto){
   texto = texto.toLowerCase();
   for(i=0; i<texto.length; i++){
      if (letras.indexOf(texto.charAt(i),0)!=-1){
         return 1;
      }
   }
   return 0;
} 

function tiene_minusculas(texto){
   for(i=0; i<texto.length; i++){
      if (letras.indexOf(texto.charAt(i),0)!=-1){
         return 1;
      }
   }
   return 0;
} 

function tiene_mayusculas(texto){
   for(i=0; i<texto.length; i++){
      if (letras_mayusculas.indexOf(texto.charAt(i),0)!=-1){
         return 1;
      }
   }
   return 0;
} 
function validarRadio(obj){

	var val_checked = 0;
	for(var i=0; i< obj.length;i++){
		if(obj[i].checked) { val_checked = obj[i].value;}
	}
	return val_checked;
}