jQuery(function ($) {

    var k = 'cookiepolicy',
        v = 'accepted',
        p = $('#cookie-policy'),
        h = p.height();
    // Ocultamos el aviso movi�ndolo tantos PX como mida de alto con el contenido actual.
    p.css('bottom', -1 * h).removeClass('cp-hidden');

    // Si no existe la cookie o no se ha aceptado la pol�tica de cookies, mostramos el mensajito.
    if ($.cookie(k) !== v) {
        p.on('click', 'a.cp-close', function (e) {
            // Evitamos que se propague el evento.
            e.preventDefault();
            // Ocultamos el mensaje.
            p.animate({bottom: -1 * h}, 300, 'easeInOutCirc');
            // Creamos la cookie.
            $.cookie(k, v, {expires: 365, path: '/'});
        }).animate({bottom: 0}, 1200, "easeOutBounce");
    }

});

$(window).resize(function() {
	p = $('#cookie-policy'),
	h = p.height();
	p.css('bottom', -1 * h).removeClass('cp-hidden');
});
