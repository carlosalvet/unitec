// -*- mode: javascript -*-

dodo.docImportNode = function (doc, node, deep) {
	if (!doc.importNode) {
		var target;

		switch (node.nodeType) {
		case 1:
			target = doc.createElement (node.nodeName);
			for (var i = 0; i < node.attributes.length; i++) {
				var attr = node.attributes[i];
				if (attr.name == "style")
					target.style.cssText = attr.value;
				else
					target.setAttribute (attr.name, attr.value);
			}
			break;
		case 3:
			target = doc.createTextNode (node.nodeValue);
		}
		
		if (deep && node.hasChildNodes ())
			for (var child = node.firstChild; child; child = child.nextSibling)
				target.appendChild (dodo.docImportNode (doc, child, true));
		return target;
	}
	return doc.importNode (node, deep);
}

dodo.renderXml = function (node, onlyInner) {
	switch (node.nodeType) {
	case 1:
		var html = '';
		if (!onlyInner) {
			html = '<' + node.nodeName;
			for (var i = 0, attr; attr = node.attributes[i]; i++)
				html += " " + attr.name + '="' + attr.value + '"';
			if (node.childNodes.length == 0)
				html += ' /';
			html += '>';
		}

		for (var i = 0, child; child = node.childNodes[i]; i++)
			html += dodo.renderXml (child);

		if (!onlyInner && node.childNodes.length > 0)
			html += '</' + node.nodeName + '>';

		return html;
	case 3:
		return node.nodeValue;
	}
	return "";
}

dodo.nodeLastChild = function (node) {
	if (node.hasChildNodes ()) {
		return node.childNodes[node.childNodes.length - 1];
	}
	return null;
}

dodo.nodeReplace = function (node, nodeName, values, idx) {
	if (!idx)
		idx = 0;
	if (idx < values.length) {
		if (node.nodeName.toLowerCase () == nodeName) {
			var value = values[idx++];
			if (value != undefined)
				node.innerHTML = value;
		} else
			for (var i = 0; i < node.childNodes.length; i++) {
				idx = dodo.nodeReplace (node.childNodes[i], nodeName, values, idx);
				if (idx == values.length)
					break;
			}
	}
	return idx;
}

dodo.nodeIsParentOf = function (parent, node) {
	for (; node; node = node.parentNode)
		if (node == parent)
			return true;
	return false;
}

dodo.adminClassDisplay = function (tagName, display) {
	var nodes = document.getElementsByTagName (tagName);
	for (var i = 0; i < nodes.length; i++) {
		var node = nodes[i];
		if (node.className.indexOf ("admin") == 0 ||
			node.className.indexOf (" admin") != -1)
			node.style.display = display;
	}
}

dodo.replaceLinkTargets = function (doc) {
	var links = doc.getElementsByTagName ("a");
	for (var i = 0; i < links.length; i++) {
		var link = links[i];
		if (link.target == "") {
			if (link.className.indexOf ("new-win") != -1) {
				link.target = "_blank";
			}
		}
	}
}

dodo.httpReqGet = function () {
	if(window.ActiveXObject) {
       		try {
        		return new ActiveXObject("Msxml2.XMLHTTP");
      		} catch(e) {
        		try {
          			return new ActiveXObject("Microsoft.XMLHTTP");
        		} catch(e) {}
		}
	} else if (window.XMLHttpRequest && !(window.ActiveXObject)) {
    		try {
			return new XMLHttpRequest();
		} catch(e) {}
	}

	return undefined;
}

dodo.httpReqLoad = function (req, url, callback, bypassCache, parms, method)
{
	req.onreadystatechange = callback;

	var flags;
	if (bypassCache == undefined || bypassCache)
		url += (url.match(/\?/) == null ? "?" : "&") + (new Date()).getTime();
	if (!method)
		method = "GET";

	parms_str = "";
	if (parms) {
		for (var key in parms)
			parms_str += key + "=" + encodeURIComponent (parms[key]) + "&";
		parms_str = parms_str.substr (0, parms_str.length - 1);
		if (method == "GET")
			url += "?" + parms_str;
	}

	req.open (method, url, true);
	if (method == "POST") {
		req.setRequestHeader ("Content-type", "application/x-www-form-urlencoded");
		req.setRequestHeader ("Content-length", parms_str.length);
		req.setRequestHeader ("Connection", "close");
		req.send (parms_str);
	} else
		req.send ("");
}

dodo.cssProp2JS = function (prop) {
	var offset;
	while ((offset = prop.indexOf ("-")) != -1)
		prop = prop.substr (0, offset) + prop.substr (offset + 1, 1).toUpperCase () + prop.substr (offset + 2);
	return prop;
}

dodo.getComputedStyle = function (node, prop) {
	var view = node.ownerDocument.defaultView;
	if (view)
		return view.getComputedStyle (node, "").getPropertyValue (prop);
	prop = dodo.cssProp2JS (prop);
	var current = node.currentStyle[prop];
	if (current == "auto")
		return node.runtimeStyle[prop];
	return current;
}