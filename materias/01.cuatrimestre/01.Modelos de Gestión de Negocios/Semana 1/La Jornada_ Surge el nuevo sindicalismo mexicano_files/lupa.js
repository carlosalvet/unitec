// -*- mode: javascript; coding: utf-8 -*-

var lupa = {
	THRESHOLD_FONT_SIZE: 10.1,

	mouseIn: function (node) {
		node.onmouseover = null;
		node.style.height = node.offsetHeight + "px";
		var html = node.innerHTML;
		node.innerHTML = '<div class="lupa" onmouseout="lupa.mouseOut (this, event)">' + html + '</div>';
		node.style.position = "relative";

		if (node.className != "rayuela-box") {
			switch (dodo.browserDetect.browser) {
			case "Safari":
				break;
			case "Explorer":
			default:
				var ads = document.getElementById ("rayuela-ads");
				if (ads)
					ads.style.visibility = "visible";
			}
		}
	},

	mouseOut: function (node, evt) {
		if (!evt)
			evt = window.event;
		if (!evt.target) {
			evt.target = evt.srcElement;
			evt.relatedTarget = evt.fromElement;
		}
		if (evt.target != node)
			return;
		if (evt.relatedTarget.parentNode == node)
			return;

		var parent = node.parentNode;
		if (parent) {
			parent.style.height = "";
			parent.style.position = "";
			parent.innerHTML = node.innerHTML;
			parent.onmouseover = function () { lupa.mouseIn (parent); };
		}

		var ads = document.getElementById ("rayuela-ads");
		if (ads)
			ads.style.visibility = "visible";
	},

	setup: function (node) {
		if (node.nodeName != "div" && node.nodeName != "DIV")
			return;

		var force = false;
		switch (node.className) {
		case "credito-primera": // classes we want to ignore
		case "penta":
		case "body-text":
		case "body-text s-s":
			return;
		case "rayuela-box": // classes we want to force
		case "pie-foto":
			force = true;
		}
		switch (node.id) {
		case "rayuela-ads": // nodes with these ids are ignored
			return;
		}
		
		var font_size;
		if (!force)
			font_size = parseFloat (dodo.getComputedStyle (node, "font-size").replace ("px", ""));

		if (force || font_size < lupa.THRESHOLD_FONT_SIZE)
			node.onmouseover = function () { lupa.mouseIn (node); };
		else
			for (var i = 0, child; child = node.childNodes[i]; i++)
				lupa.setup (child);
	}
}
