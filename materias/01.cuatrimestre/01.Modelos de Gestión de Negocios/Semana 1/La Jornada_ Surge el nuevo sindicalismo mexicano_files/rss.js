// -*- mode: javascript; coding: utf-8 -*-

var rss = {
	MAX_NEWS_ITEMS: 5,
	DATE_PARSER: new RegExp ('([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})Z'),

	xml: {},

	render: function (data, node) {
		var html = "";
		if (data.xml) {
			if (data.render)
				html = data.render;
			else {
				var xml = data.xml;
				var items = xml.getElementsByTagName ("item");
				for (var i = 0, item; (item = items[i]) && i < rss.MAX_NEWS_ITEMS; i++) {
					var info = {date: ""};
					for (var j = 0, child; child = item.childNodes[j]; j++) {
						switch (child.nodeName) {
						case "title":
							info.title = child.firstChild.nodeValue;
							if (!info.title)
								info.title = child.firstChild.firstChild.nodeValue;
							if (!info.title)
								info.title = " ";
							var code = info.title.charCodeAt (info.title.length - 1);
							if ((code >= 97 && code <= 122) || (code >= 65 && code <= 90))
							        info.title += ".";
							break;
						case "link":
							info.href = child.firstChild.nodeValue;
							break;
						case "dc:date":
							var arr = rss.DATE_PARSER.exec (child.firstChild.nodeValue);
							if (arr) {
								var date = new Date (parseInt (arr[1]), parseInt (arr[2]) - 1, parseInt (arr[3]), 
									parseInt (arr[4]), parseInt (arr[5]), parseInt (arr[6]));
								date.setTime (date.getTime () - (6 * 60 * 60 * 1000));
								var mins = date.getMinutes ().toString ();
								if (mins < 10) mins = "0" + mins;
								info.date = '<span class="time">' + date.getHours () + ":" + mins + '</span>';
							}
							break;
						}
					}
					if (info.title && info.href)
						html += '<li>' + info.date + '<a href="' + info.href + '">' + info.title + '</a></li>';
				}
				data.render = html;
			}
		}

		if (node.push)
			for (var i = 0; n = node[i]; i++)
				n.innerHTML = html;
		else
			node.innerHTML = html;
	},

	onLoad: function (req, node, id) {
		if (req.readyState == 4) {
			if (req.status == 200 || req.status == 0) {
				data = { xml: req.responseXML };
				if (id)
					rss.xml[id] = data;
				
				if (node)
					rss.render (data, node);
			}

			//dodo.finishTasks (1, "rss " + req.responseXML.documentURI + " " + req.status);
		}
	},

	load: function (node, src, id) {
		var req = dodo.httpReqGet ();
		dodo.httpReqLoad (req, src, function () { rss.onLoad (req, node, id); });
	}
}
