window.rmLightboxDivs = function(){	
	var rmBody = document.getElementsByTagName('body').item(0);  
  	var rmDivOverlay = document.getElementById('overlayind');
  	var rmDivLightboxind = document.getElementById('lightboxind');
  	rmBody.removeChild(rmDivOverlay);
  	rmBody.removeChild(rmDivLightboxind);
}

window.getPageScrollInd = function(){

	var yScroll;

	if (self.pageYOffset) {
		yScroll = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop){	 // Explorer 6 Strict
		yScroll = document.documentElement.scrollTop;
	} else if (document.body) {// all other Explorers
		yScroll = document.body.scrollTop;
	}

	arrayPageScroll = new Array('',yScroll) 
	return arrayPageScroll;
}

window.getPageSizeInd = function(){
	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}


	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}

window.pauseInd = function(numberMillis) {
	var now = new Date();
	var exitTime = now.getTime() + numberMillis;
	while (true) {
		now = new Date();
		if (now.getTime() > exitTime)
			return;
	}
}

window.getKeyInd = function(e){
	if (e == null) { // ie
		keycode = event.keyCode;
	} else { // mozilla
		keycode = e.which;
	}
	key = String.fromCharCode(keycode).toLowerCase();
	
	if(key == 'x'){ hideLightboxind(); }
}

window.listenKey = function() {	document.onkeypress = getKeyInd; }

window.showLightboxind = function(objLink) {
	var objOverlayind = document.getElementById('overlayind');
	var objLightboxind = document.getElementById('lightboxind');
	var objCaption = document.getElementById('lightboxindCaption');
	var objLoadingImage = document.getElementById('loadingImage');	
	
	var arrayPageSize = getPageSizeInd();
	var arrayPageScroll = getPageScrollInd();

	// center loadingImage if it exists
	if (objLoadingImage) {
		objLoadingImage.style.top = (arrayPageScroll[1] + ((arrayPageSize[3] - 35 - objLoadingImage.height) / 2) + 'px');
		objLoadingImage.style.left = (((arrayPageSize[0] - 20 - objLoadingImage.width) / 2) + 'px');
		objLoadingImage.style.display = 'block';
	}

	// set height of Overlayind to take up whole page and show
	objOverlayind.style.height = (arrayPageSize[1] + 'px');
	objOverlayind.style.display = 'block';
        

	// preload image
	imgPreload = new Image();

	imgPreload.onload=function(){
               
		//objImage.src = objLink.name;

		// center lightboxind and make sure that the top and left values are not negative
		// and the image placed outside the viewport
		var lightboxindTop = (arrayPageScroll[1] + ((arrayPageSize[3] - 35 - imgPreload.height) / 2) / 5);
		var lightboxindLeft = ((arrayPageSize[0] - 20 - imgPreload.width) / 2) - 150;
		
		objLightboxind.style.top = (lightboxindTop < 0) ? "0px" : lightboxindTop + "px";
		objLightboxind.style.left = (lightboxindLeft < 0) ? "0px" : lightboxindLeft + "px";
		
		// A small pause between the image loading and displaying is required with IE,
		// this prevents the previous image displaying for a short burst causing flicker.
		if (navigator.appVersion.indexOf("MSIE")!=-1){
			pauseInd(250);
		} 

		if (objLoadingImage) {	objLoadingImage.style.display = 'none'; }

		// Hide select boxes as they will 'peek' through the image in IE
		selects = document.getElementsByTagName("select");
        	for (i = 0; i != selects.length; i++) {
                	selects[i].style.visibility = "hidden";
        	}

	       	objLightboxind.style.display = 'block';

		// After image is loaded, update the overlayind height as the new image might have
		// increased the overall page height.
		arrayPageSize = getPageSizeInd();
		objOverlayind.style.height = (arrayPageSize[1] + 'px');
		
		// Check for 'x' keypress
		listenKey();

		return false;
	}

	imgPreload.src = objLink.name;
	
}

window.hideLightboxind = function() {

	// get objects
	objOverlayind = document.getElementById('overlayind');
	objLightboxind = document.getElementById('lightboxind');

	// hide lightboxind and overlayind
	objOverlayind.style.display = 'none';
	objLightboxind.style.display = 'none';

	// make select boxes visible
	selects = document.getElementsByTagName("select");
        for (i = 0; i != selects.length; i++) {
		selects[i].style.visibility = "visible";
	}

	// disable keypress listener
	document.onkeypress = '';
	
	rmLightboxDivs();
	//document.getElementById("idAhrefCarousel").onmouseout = rmLightboxDivs(); 	
	
}

window.initLightboxind = function() {	
	
	if (!document.getElementsByTagName){ return; }
	var anchors = document.getElementsByTagName("a");

	// loop through all anchor tags
	for (var i=0; i<anchors.length; i++){
		var anchor = anchors[i];		

		if (anchor.getAttribute("name") && (anchor.getAttribute("rel") == "lightboxind")) {
			anchor.onclick = function () {
				showLightboxind(this);
				return false;
			}
		}
	}
	
	var objBody = document.getElementsByTagName("body").item(0);
	
	// create overlayind div and hardcode some functional styles (aesthetic styles are in CSS file)
	var objOverlayind = document.createElement("div");
	objOverlayind.setAttribute('id','overlayind');
	objOverlayind.onclick = function () {hideLightboxind(); return false;}
	objOverlayind.style.display = 'none';
	objOverlayind.style.position = 'absolute';
	objOverlayind.style.top = '0';
	objOverlayind.style.left = '0';
	objOverlayind.style.zIndex = '90';
 	objOverlayind.style.width = '100%';
	objBody.insertBefore(objOverlayind, objBody.firstChild);
	
	var arrayPageSize = getPageSizeInd();
	var arrayPageScroll = getPageScrollInd();

	// preload and create loader image
	var imgPreloader = new Image();
	
	// if loader image found, create link to hide lightboxind and create loadingimage
	imgPreloader.onload=function(){

		var objLoadingImageLink = document.createElement("a");
		objLoadingImageLink.setAttribute('href','#');
		objLoadingImageLink.onclick = function () {hideLightboxind(); return false;}
		objOverlayind.appendChild(objLoadingImageLink);
		
		var objLoadingImage = document.createElement("img");
		objLoadingImage.src = "https://www.jornada.com.mx/v8.0/images/media/loading.gif";
		objLoadingImage.setAttribute('id','loadingImage');
		objLoadingImage.style.position = 'absolute';
		objLoadingImage.style.zIndex = '150';
		objLoadingImageLink.appendChild(objLoadingImage);

		imgPreloader.onload=function(){};	//	clear onLoad, as IE will flip out w/animated gifs

		return false;
	}

	imgPreloader.src = "https://www.jornada.com.mx/v8.0/images/media/loading.gif";
	// create lightboxind div, same note about styles as above
	var objLightboxind = document.createElement("div");
	objLightboxind.setAttribute('id','lightboxind');
	objLightboxind.style.display = 'none';
	objLightboxind.style.position = 'absolute';
	objLightboxind.style.zIndex = '100';
	var objLightIframe = document.createElement("iframe");
	objLightIframe.setAttribute('id','lightboxindIframe');
	objLightIframe.setAttribute('width','597px');
	objLightIframe.setAttribute('height','410px');
	objLightIframe.setAttribute('marginwidth','0');
	objLightIframe.setAttribute('marginheight','0');
	objLightIframe.setAttribute('scrolling','no');
	objLightIframe.setAttribute('frameborder','0');
	objLightIframe.setAttribute('src','https://www.jornada.com.mx/data/issuu/indignados.html');	
	objLightboxind.appendChild(objLightIframe);

	objBody.insertBefore(objLightboxind, objOverlayind.nextSibling);
	
	// create link
	var objLink = document.createElement("a");
	objLink.setAttribute('href','javascript:void(0)');
	objLink.setAttribute('title','Cerrar');
	//objLink.style.display = 'none';
	objLink.onclick = function () {hideLightboxind(); return false;}
	objLightboxind.appendChild(objLink);

	// preload and create close button image
	var imgPreloadCloseButton = new Image();

	// if close button image found, 
	imgPreloadCloseButton.onload=function(){

		var objCloseButton = document.createElement("img");
		objCloseButton.src = "https://www.jornada.com.mx/v8.0/images/media/close.gif";
		objCloseButton.setAttribute('id','closeButton');
		objCloseButton.style.position = 'absolute';
		objCloseButton.style.zIndex = '200';
		objLink.appendChild(objCloseButton);

		return false;
	}

	imgPreloadCloseButton.src = "https://www.jornada.com.mx/v8.0/images/media/close.gif";
}