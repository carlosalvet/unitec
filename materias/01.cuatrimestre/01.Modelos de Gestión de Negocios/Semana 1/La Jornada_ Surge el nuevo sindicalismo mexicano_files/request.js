// -*- mode: javascript -*-

dodo.CACHE_NO_SCRIPTS = true;

dodo.scriptSendRequest = function (doc, url, onLoad) {
	script = doc.createElement ("script");
	if (dodo.CACHE_NO_SCRIPTS) {
		if (url.indexOf ("?") == -1)
			url += "?";
		else
			url += "&nocache_date=";
		url += (new Date () * 1);
	}
	if (onLoad) {
		script.onload = onLoad;
		// for IE:
		if (document.ie7init)
			script.onreadystatechange = function () { if (script.readyState == "loaded") onLoad (); };
	}
	script.src = url;
	doc.getElementsByTagName ("head")[0].appendChild (script);
}

dodo.dateEpochSerialize = function (date) {
	return parseInt (date.valueOf ().toString ().substr (0, 10));
}

dodo.dateEpochDeserialize = function (num) {
	return new Date (parseInt (num + "00"));
}

dodo.parmsSerialize = function (parms) {
	var str = "";
	for (var key in parms) {
		var value = String (parms[key]);
		key = key.replace (/=/g, "\\=");
		key = key.replace (/,/g, "\\,");
		//		key = key.replace (/:/g, "\\:");
		value = value.replace (/=/g, "\\=");
		value = value.replace (/,/g, "\\,");
		value = value.replace (/:/g, "\\:");
		str += key + "=" + value + ",";
	}
	str = encodeURI (str).replace (/&/g, "%26");
	str = str.replace (/#/g, "%23");
	return str.replace (/;/g, "%3B");
}

dodo.challengeRequest = function (doc, username, resource, parms, target, name) {
	var url = "/cgi-bin/request?ch_user=" + username + "&ch_res=" + resource;
	if (parms)
		url += "&ch_parms=" + dodo.parmsSerialize (parms);
	if (target)
		url += "&target=" + target;
	if (name)
		url += "&name=" + name;
	dodo.scriptSendRequest (doc, url);
}

dodo.challengeReply = function (doc, id, hash, isScript, target, name) {
	var url = "/cgi-bin/reply"; 
	if (name)
		url += "/" + name;
	url += "?ch_id=" + id + "&ch_reply=" + hash;
	if (target)
		doc = dodo.frameDocument (doc, target);
	if (isScript)
		dodo.scriptSendRequest (doc, url);
	else
		doc.location = url;
}

dodo.challengeReplyPost = function (doc, id, hash, form_id) {
	var form = doc.forms[form_id];
	form.action = "/cgi-bin/reply";
	form.elements["ch_id"].value = id;
	form.elements["ch_reply"].value = hash;
	form.submit ();
}

dodo.challengeFailed = function () { // default behaviour
//	dodo.loginReport ("Challenge failed");
}

dodo.replyFailed = function () { // default behaviour
//	dodo.loginReport ("Reply failed");
}
