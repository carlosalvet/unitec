// -*- mode: javascript -*-

var dodo = {};

if (!window.console) {
	window.console = {
		log: function (str) { document.getElementById ("dodo-console").innerHTML += "<div>" + str + "</div>"; }
	};
}

dodo.ONLOAD_ARRAY = new Array ();
dodo.ONLOADFINISH_ARRAY = new Array ();
dodo.ONRESIZE_ARRAY = new Array ();
dodo.ONTASKSFINISH_ARRAY = new Array ();
dodo.TASKS = 0;

dodo.contentRefresh = function () {
	dodo.calcContentHeight ();
	if (document.recalc)
		document.recalc ();
}

dodo.handlersCall = function (array, arg) {
	for (var i = 0; i < array.length; i++)
		if (array[i] != null)
			array[i] (arg);
}

dodo.handlersPush = function (array, func) {
	array.push (func);
	return array.length;
}

dodo.delayRun = function (func, delay) {
	if (!delay)
		delay = 1;
	return window.setTimeout (func, delay);
}

dodo.clearTimeout = function (id) {
	return window.clearTimeout (id);
}

dodo.windowOnResize = function (event) {
	dodo.handlersCall (dodo.ONRESIZE_ARRAY, event);
}

dodo.bodyOnLoad = function (event) {
	window.onresize = dodo.windowOnResize;

	if (!top.SITE)
		top.SITE = {noLogin: true};
	top.SITE.document = document;

	if (window.IE7)
		window.IE7.CSS.print = { recalc: function () {} };

	dodo.handlersCall (dodo.ONLOAD_ARRAY, event);
	dodo.handlersCall (dodo.ONLOADFINISH_ARRAY, event);
}

dodo.startTasks = function (num, debug) {
	dodo.TASKS += num;
//	console.log ("start " + dodo.TASKS + " " + debug);
	return dodo.TASKS;

}

dodo.finishTasks = function (num, debug) {
	if (!num) num = 1;
	dodo.TASKS -= num;
//	console.log ("finish " + dodo.TASKS + " " + debug);
	if (dodo.TASKS == 0)
		dodo.delayRun (function () {
//			console.log ("finish trigger");
			dodo.handlersCall (dodo.ONTASKSFINISH_ARRAY);
		});
}

dodo.tasksPending = function () {
	return (dodo.TASKS > 0);
}

dodo.frameDocument = function (doc, id) {
	var frame = doc.getElementById (id);
	return frame.contentDocument? frame.contentDocument: doc.frames[id].document;
}

dodo.frameWindow = function (doc, id) {
	var frame = doc.getElementById (id);
	return frame.contentWindow? frame.contentWindow: doc.frames[id];
}

dodo.onLoadPush = function (func) {
	return dodo.handlersPush (dodo.ONLOAD_ARRAY, func);
}

dodo.onLoadFinishPush = function (func) {
	return dodo.handlersPush (dodo.ONLOADFINISH_ARRAY, func);
}

dodo.onResizePush = function (func) {
	return dodo.handlersPush (dodo.ONRESIZE_ARRAY, func);
}

dodo.onTasksFinishPush = function (func) {
	return dodo.handlersPush (dodo.ONTASKSFINISH_ARRAY, func);
}

dodo.cookieSet = function (c_name, value, expiredays) {
	var exdate = new Date ();
	exdate.setDate (exdate.getDate () + expiredays);
	document.cookie = c_name + "=" + encodeURI (value) +
		((expiredays == null)? "" : ";expires=" + exdate.toGMTString () + ";path=/");
}

dodo.cookieGet = function (c_name) {
	if (document.cookie.length > 0) {
		var c_start = document.cookie.indexOf (c_name + "=");
		if (c_start != -1) { 
			c_start = c_start + c_name.length + 1;
			var c_end = document.cookie.indexOf (";", c_start);
			if (c_end == -1) 
				c_end = document.cookie.length;
			return decodeURI (document.cookie.substring (c_start, c_end));
		}
	}
	return "";
}

dodo.argsParse = function (loc) {
	if (loc.search) {
		var args = {};
		var pairs = loc.search.substr (1).split (/[;&]/);
		for (var i = 0; i < pairs.length; i++) {
			var keyval = pairs[i].split ("=");
			args[keyval[0]] = decodeURI (keyval[1]);
		}
		return args;
	}
	return undefined;
}

dodo.hashSort = function (hash, key, asc) {
	hash.sort (function (a, b) {
		if (a[key] < b[key]) return -1 * asc;
		if (a[key] > b[key]) return 1 * asc;
		return 0;
	});
}
