// -*- mode: javascript; coding: utf-8 -*-

var sumario = {
    SECT_LABELS: {
	politica:"Política",
	economia:"Economía",
	mundo:"Mundo",
	sociedad:"Sociedad",
	capital:"Capital",
	estados:"Estados",
	cultura:"Cultura",
	espectaculos:"Espectáculos",
	deportes:"Deportes",
	ciencias:"Ciencias",
	opinion:"Opinión",
	columnas:"Columnas"
	},

    TITLE_Y_POS: {
	politica: 100,
	economia: 182,
	mundo: 300,
	sociedad: 382,
	capital: 50,
	estados: 422,
	cultura: 534,
	espectaculos: 596,
	deportes: 702,
	opinion: 904,
	        columnas: 844,
	       ciencias: 762
	},

    ICN_Y_POS: {
	politica: 1104,
	economia: 1135,
	mundo: 1072,
	sociedad: 1041,
	capital: 1010,
	estados: 978,
	cultura: 946,
	espectaculos: 915,
	deportes: 822,
	opinion: 853,
	        columnas: 790,
	        ciencias: 884
	},

    matchUrl: function () {
	
	var url = window.location.pathname;   
	var matching = /index/gi;
	var matched = url.match(matching);
	return matched;
	
	},
    
    urlHaveSec: function () {

	var url = window.location.pathname;
	var urlhavesect = url.substring(12);
                urlhavesect = urlhavesect.substring(0, urlhavesect.length-9);
	return urlhavesect;

	},
    
    urlExtract: function () {

	var url = window.location.pathname;
                var urlextractlastpart;
	if (!window.CIMPRESADAY)
	    urlextractlastpart = url.substring(0,12);
	else
	    urlextractlastpart = window.CIMPRESADAY;
	
	return urlextractlastpart;

	},

    renderSectTitle: function (sect, text) {
	
	var matchTitleSec = sumario.matchUrl();
	html = '<div class="title">' +
	            '<div class="summary icon" style="' + 
                        'background-position: 0px -' + sumario.ICN_Y_POS[sect] + 'px;"> </div>';

	if (matchTitleSec != null) {

	    html +='<a class="summary ref title" href="' + ((sect == 'columnas')? 'opinion': sect) + '">' + text + '</a></div>';

	    } else {
		
		var urlabs = sumario.urlExtract();
		html +='<a class="summary ref title" href="' + urlabs + ((sect == 'columnas')? 'opinion': sect) + '">' + text + '</a></div>';

		}

	html += '<div class="banner gui" style="' + 
	    'background-position: 48px -' + sumario.TITLE_Y_POS[sect] + 'px"><img src="/v7.0/imagenes/gradient.png" alt="" /></div>';

	return html
	},

    renderPleca: function () {
	return '<div class="credito-primera"><div class="pleca">' +
	    '</div><span class="pleca-spacer">&nbsp;</span></div>';
	},

    renderSectItems: function (xml, sect, start, maxItems, byType) {
	
	var matchHaveIndex = sumario.matchUrl();
	var html = "<ul>"
	var items = xml.getElementsByTagName ("Item");
	for (var i = start, count = 0, item; (item = items[i]) && count < maxItems; i++) {
	    
var item_type = item.getAttribute ("type");
if(item_type != "Opinion"){
	    var item_sect = item.getAttribute ("section");
	    if ((byType && ((byType.test && byType.test (item_type)) || item_type == byType) && 
		      item_sect != "edito" && 
		      item_sect != "correo") ||
		    item_sect == sect) {

		var data = { section: item_sect, type: item_type };
		for (var j = 0, child; child = item.childNodes[j]; j++) {
		    if (!child.nodeName)
			continue;
		    var val = dodo.renderXml (child, true);
		    switch (child.nodeName) {
			case "author":
			data.author = val;
			break;
			case "title":
			data.title = val;
			break;
			}
		    }

		count ++;

		if (matchHaveIndex != null) {
		    
		    html += '<li><a data-cd="1" href="' + ((sect == 'columnas')? 'opinion': sect) + '/' + item.getAttribute ("id") + '">';
		    
		    } else {
			
			var haveSect = sumario.urlHaveSec();
			if (!haveSect) {
			    
			    if (window.CIMPRESADAY) 
				html += '<li><a data-cd="2"  href="' + window.CIMPRESADAY  + ((sect == 'columnas')? 'opinion': sect) + '/' + item.getAttribute ("id") + '">';
			    else

				html += '<li><a data-cd="3"  href="' + ((sect == 'columnas')? 'opinion': sect) + '/' + item.getAttribute ("id") + '">';

			    } else {
				
				html += '<li><a data-cd="4"  href="' + item.getAttribute ("id") + '">';

			    }

			

			}
		if (byType) {
		    if (data.author)
			html += data.author + ': ';
		    html += '<b>' + data.title + '</b></a></li>';
		    } else
			html += data.title + '</a></li>';
		}
}
	    }
	html += '</ul>';

	return html;
	},

    renderSect: function (xml, sect, cols, start, num, maxItems, byType) {
	var html = '';
	if (num % cols == 0)
	    html += "<tr>";

	html += "<td>" + sumario.renderSectTitle (sect, sumario.SECT_LABELS[sect]) + sumario.renderSectItems (xml, sect, start, maxItems, byType) + "</td>";
	if (num % cols == cols - 1) {
	    html += '</tr><tr>';
	    for (var i = 0; i < cols; i++)
		html += '<td>' + sumario.renderPleca () + '</td>';
	    html += '</tr>';
	    }

	return html;
	}
,
// -->>>>>
renderSectItemsOpi: function (xml, sect, start, maxItems, byType) {
    
    var matchHaveIndex = sumario.matchUrl();
    var html = "<ul>"
    var items = xml.getElementsByTagName ("Item");
    for (var i = start, count = 0, item; (item = items[i]) && count < maxItems; i++) {
	    
var item_type = item.getAttribute ("type");
//if(item_type != "Opinion"){
	    var item_sect = item.getAttribute ("section");
	    if ((byType && ((byType.test && byType.test (item_type)) || item_type == byType) && 
		       item_sect != "edito" && 
		       item_sect != "correo") ||
		    item_sect == sect) {

		var data = { section: item_sect, type: item_type };
		for (var j = 0, child; child = item.childNodes[j]; j++) {
		        if (!child.nodeName)
			    continue;
		        var val = dodo.renderXml (child, true);
		        switch (child.nodeName) {
			    case "author":
			    data.author = val;
			    break;
			    case "title":
			    data.title = val;
			    break;
			    }
		        }

		count ++;

		if (matchHaveIndex != null) {
		        
		        html += '<li><a data-cd="1" href="' + ((sect == 'columnas')? 'opinion': sect) + '/' + item.getAttribute ("id") + '">';
		        
		        } else {
			    
			    var haveSect = sumario.urlHaveSec();
			    if (!haveSect) {
				    
				    if (window.CIMPRESADAY) 
					html += '<li><a data-cd="2"  href="' + window.CIMPRESADAY  + ((sect == 'columnas')? 'opinion': sect) + '/' + item.getAttribute ("id") + '">';
				    else

					html += '<li><a data-cd="3"  href="' + ((sect == 'columnas')? 'opinion': sect) + '/' + item.getAttribute ("id") + '">';

				    } else {
					
					html += '<li><a data-cd="4"  href="' + item.getAttribute ("id") + '">';

					    }

			    

			    }
		if (byType) {
		        if (data.author)
			    html += data.author + ': ';
		        html += '<b>' + data.title + '</b></a></li>';
		        } else
			    html += data.title + '</a></li>';
		}

	    }
    html += '</ul>';

    return html;
    },

    renderSectOpi: function (xml, sect, cols, start, num, maxItems, byType) {
	var html = '';
	if (num % cols == 0)
	        html += "<tr>";

	html += "<td>" + sumario.renderSectTitle (sect, sumario.SECT_LABELS[sect]) + sumario.renderSectItemsOpi (xml, sect, start, maxItems, byType) + "</td>";
	if (num % cols == cols - 1) {
	        html += '</tr><tr>';
	        for (var i = 0; i < cols; i++)
		    html += '<td>' + sumario.renderPleca () + '</td>';
	        html += '</tr>';
	        }

	return html;
	}

// -->>>>
}
