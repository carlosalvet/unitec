// -*- mode: javascript -*-

var main = {
	SUBMENU_REMOVE_TIMEOUT: 5000, // ms
	CALENDAR_FADE_RATE: 0.8,
	CALENDAR_FADE_SPEED: 10, // ms
	
	showMenu: function (event, node, pos) {
		if (event) {
			var className = "selected";
			
			if (event.timeStamp - node.timeStamp < 500)
				return false;
			node.timeStamp = event.timeStamp;
		}

		if (!event || node.className.indexOf ("selected") != -1) {
			className = "";
			pos = -1;
		}

		for (var i = 0, sib; sib = node.parentNode.childNodes[i]; i++)
			sib.className = (sib.className.indexOf ("final") == -1)? "": "final";
		node.className = className;

		var subs = document.getElementById ("main-submenus");
		for (var i = 0, sub; sub = subs.childNodes[i]; i++) {
			if (i == pos) {
				var submenu = sub;
				var menu = node;
				sub.style.display = "block";
				sub.onmouseout = function () { main.submenuOut (menu, submenu); };
				sub.onmousemove = function () { main.submenuMove (submenu); };
				main.submenuOut (menu, submenu);
			} else {
				sub.timeout = undefined;
				if (sub.style)
					sub.style.display = "none";
			}
		}

		return false;
	},

	submenuOut: function (menu, submenu) {
		submenu.timeout = window.setTimeout (function () { 
			if (submenu.timeout) {
				submenu.timeout = undefined; 
				main.showMenu (undefined, menu); 
			}
		}, main.SUBMENU_REMOVE_TIMEOUT);
	},

	submenuMove: function (submenu) {
		if (submenu.timeout) {
			window.clearTimeout (submenu.timeout);
			submenu.timeout = undefined;
		}
	},

	setBg: function () {
		if (!window.JORNADA_LIGHT)
			document.getElementById ("main-cont").style.backgroundColor = "transparent";
	},

	adsSideShow: function (show) {
		var div = document.getElementById ("side-ads");
		if (div)
			div.style.visibility = show? "visible": "hidden";		       
		var div = document.getElementById ("srvices-space");
		if (div)
			div.style.visibility = show? "hidden": "visible";		       	
	},

	srvicesSideShow: function (show) {
		var div = document.getElementById ("side-actions");
		if (div)
			div.style.visibility = show? "visible": "hidden";		       
	},

	footerdShow: function (show) {
		var div = document.getElementById ("footer-banner");
		if (div)
			div.style.visibility = show? "visible": "hidden";		       
	},

	calendarShow: function (div, opacity) {
		if (!opacity) {
			opacity = 1;
			div.style.display = "block";
			main.adsSideShow (false);
		}

		if (document.ie7init || opacity < 0.1) {
			div.style.opacity = 1;
			return;
		}
			
		opacity *= main.CALENDAR_FADE_RATE;
		div.style.opacity = (1 - opacity).toString ();
		dodo.delayRun (function () { main.calendarShow (div, opacity); }, main.CALENDAR_FADE_SPEED);
	},

	calendarHide: function (div, opacity) {
		if (document.ie7init || (opacity && opacity < 0.1)) {
			div.style.opacity = 0;
			div.style.display = "none";
			main.adsSideShow (true);
			return;
		}
			
		if (!opacity)
			opacity = 1;
		opacity *= main.CALENDAR_FADE_RATE;
		div.style.opacity = opacity.toString ();
		dodo.delayRun (function () { main.calendarHide (div, opacity); }, main.CALENDAR_FADE_SPEED);
	},

	calendarImgOnLoad: function (evt) {
		if (!evt) evt = window.event;
		this.parentNode.style.display = (evt.type == "error")? "none": "inline";
	},

	calendarToggle: function () {
		var calDiv = document.getElementById ("main-fecha-calendar");
		var div = calDiv.parentNode.parentNode;
		if (div.style.display == "block") {
			if (window.SHARETHIS) SHARETHIS.showEmbeds ();
			main.calendarHide (div);
		} else {
			if (window.SHARETHIS) SHARETHIS.hideEmbeds ();
			main.calendarShow (div);
			if (!calDiv.firstChild) {
				div.firstChild.lastChild.onclick = main.calendarToggle;
				var a = calDiv.previousSibling.firstChild;
				var img = a.firstChild;

				var today = new Date ();
				var cur_date = new Date (window.MAIN_CWD.substr (1, window.MAIN_CWD.length - 2));
				var no_main_cwd;
				if (!cur_date.getFullYear ()) {
					no_main_cwd = true;
					cur_date = new Date ();
				}

				var cal = new fcp.Calendar (calDiv, false, cur_date);
				img.onload = main.calendarImgOnLoad;
				img.onerror = main.calendarImgOnLoad;

				var go_hoy = div.lastChild.lastChild;
				var go_hoy_click = function () {
					var t = new Date ();
					cal.set_date_time (t); 
					cal.onselect (t);
				};
				go_hoy.onclick = go_hoy_click;
				var check_hide_hoy = function (date) {
					if (today.getFullYear () == date.getFullYear () &&
					    today.getMonth () == date.getMonth () &&
					    today.getDate () == date.getDate ())
					    //go_hoy.style.visibility = "hidden";
						go_hoy.style.display = "none";
					else
					    //go_hoy.style.visibility = "visible";
						go_hoy.style.display = "block";
				}
				if (no_main_cwd)
					go_hoy_click ();
				else
					check_hide_hoy (cur_date);

				cal.onselect = function (date) { 
					check_hide_hoy (date);

					var addZero = function (n) { return (n < 10)? "0" + n: n; };
					var dateStr = date.getFullYear () + "/" + 
						addZero (date.getMonth () + 1) + "/" +
						addZero (date.getDate ());
					a.href = "/" + dateStr + "/";
					img.src = "/v7.0/imagenes/pixel.gif";
					dodo.delayRun (function () { img.src = "/" + dateStr + "/planitas/portadita.jpg"; });
					img.alt = img.title = "Portada de " + dateStr + ". Seleccione para ir a esta edición.";
				};
			}
		}
	},

	calendarClick: function () {
		if (!window.fcp)
			dodo.scriptSendRequest (document, "/v7.0/scripts/fcp_calendar.js", function () { main.calendarToggle (); });
		else
			main.calendarToggle ();
	},

        srvContactHide: function (srv) {
	    if (srv != null) {
		if (srv.className != "button sunk")
                    return false;

	    document.getElementById ("contacto-space").style.visibility = "hidden";		
		main.adsSideShow (true);
		main.srvicesSideShow (true);
		srvices.hideMainSumariosTitle(false);
		srv.className = "button";
		return true;
	    }
	},

	newIframe: function (className) {
		var iframe = document.createElement ("iframe");
		iframe.setAttribute ("scrolling", "no");
		iframe.setAttribute ("frameborder", "0");
		iframe.setAttribute ("allowtransparency", "true");
		iframe.setAttribute ("visibility", "visible");
		if (className)
			iframe.className = className;
		return iframe;
	},

	srvClickContact: function () {            
	    var srv = document.getElementById ("srv-contact");
	    var contact = document.getElementById ("contacto-space");
	    contact.style.visibility = "visible";
	    if (!contact.firstChild)
		contact.appendChild (main.newIframe ());
	    if (contact.firstChild.src.indexOf ("contact") < 0)
		contact.firstChild.src = '/v7.0/cgi/contacto.php';
	},



	onLoad: function () {
		dodo.browserDetect.init ();
		if (dodo.browserDetect.browser == "Safari" && dodo.browserDetect.version >= 4.02) {
			var nodes = document.getElementsByTagName ("object");
			for (var i = 0, node; node = nodes[i]; i++)
				if (node.getAttribute ("type") == "image/svg+xml") {
					var img = document.createElement ("img");
					img.setAttribute ("src", node.getAttribute ("data"));
					node.parentNode.replaceChild (img, node);
				}
		}

		if (document.ie7init) {
			dodo.onTasksFinishPush (document.ie7init);
			dodo.onLoadFinishPush (function () {
				if (!dodo.tasksPending ())
					document.ie7init ();
			});
		}

		var calIcon = document.getElementById ("main-calendar-icn");
		calIcon.onclick = function () { main.calendarClick (); };
	}
}

dodo.onLoadPush (main.onLoad);
