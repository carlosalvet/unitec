// -*- mode: javascript; coding: utf-8 -*-

var srvices = {
	newIframe: function (className) {
		var iframe = document.createElement ("iframe");
		iframe.setAttribute ("scrolling", "no");
		iframe.setAttribute ("frameborder", "0");
		iframe.setAttribute ("allowtransparency", "true");
		iframe.setAttribute ("visibility", "visible");
		if (className)
			iframe.className = className;
		return iframe;
	},

	location: function () {
		if (window.SRVLOCATION)
			return window.SRVLOCATION;
		var loc = window.location.toString ();
		if (window.location.hash.length > 0)
			loc = loc.substr (0, loc.indexOf (window.location.hash));
		if (window.location.search.length == 0)
			loc += '?';
		else
			loc += '&';
		return loc;
	},

	srvClickPrint: function () {
		window.print ();
	},

	srvShareHide: function (srv) {
		if (srv.className != "button sunk")
			return false;

		SHARETHIS.shareables[SHARETHIS.shareables.length - 1].popup ();
		main.adsSideShow (true);
		srv.className = "button";
		return true;
	},

	srvClickShare: function (evt, srv) {
		if (!evt) evt = window.event;
		var srv = document.getElementById ("srv-share");
		if (!srvices.srvShareHide (srv)) {
			srvices.srvMailHide (document.getElementById ("srv-mail"));
			if (document.getElementById ("srv-comment") != null)
				srvices.srvCommentHide (document.getElementById ("srv-comment"));
			main.adsSideShow (false);
			srv.className = "button sunk";
			SHARETHIS.shareables[SHARETHIS.shareables.length - 1].popup (evt);
		}
	},

        srvMailHide: function (srv) {
	    if (srv != null) {
		if (srv.className != "button sunk")
                    return false;

	    document.getElementById ("srvices-sendto").style.visibility = "hidden";
		/*SHARETHIS.showEmbeds ();*/
		main.adsSideShow (true);
		main.srvicesSideShow (true);
		srvices.hideMainSumariosTitle(false);
		srv.className = "button";
		return true;
	    }
	},

	srvClickMail: function () {
		var rndid=Math.floor(Math.random()*1000);
		urlSendMail = '/v7.0/cgi/correo.php?rndid=' + rndid; 
		var srv = document.getElementById ("srv-mail");
		if (!srvices.srvMailHide (srv)) {
     		        main.adsSideShow (false);
		        main.srvicesSideShow (false);
			srvices.srvCommentHide (document.getElementById ("srv-comment"));			
			srv.className = "button sunk";
			var sendto = document.getElementById ("srvices-sendto");
			sendto.style.visibility = "visible";
                        srvices.hideMainSumariosTitle(true);
			if (!sendto.firstChild)
				sendto.appendChild (srvices.newIframe ());
			if (sendto.firstChild.src.indexOf ("sendto") < 0)
			    sendto.firstChild.src = urlSendMail;
		}
	},

	srvCommentHide: function (srv) {
		if (srv != null){
		if (srv.className != "button sunk")
			return false;

		document.getElementById ("srvices-addcomment").style.visibility = "hidden";
		/*SHARETHIS.showEmbeds ();*/
		main.adsSideShow (true);
		main.srvicesSideShow (true);
                srvices.hideMainSumariosTitle(false);
		srv.className = "button";
		return true;
		}
	},

	srvClickComment: function () {
document.location.href = "#dd_coments";
		/*var rndid=Math.floor(Math.random()*1100);
                urlComments = '/v7.0/cgi/comentarios.php?rndid=' + rndid;
		var srv = document.getElementById ("srv-comment");
		if (!srvices.srvCommentHide (srv)) {
     		        main.adsSideShow (false);
		        main.srvicesSideShow (false);
			srvices.srvMailHide (document.getElementById ("srv-mail"));
			srv.className = "button sunk";
			var comment = document.getElementById ("srvices-addcomment");
			comment.style.visibility = "visible";
                        srvices.hideMainSumariosTitle(true);
			if (!comment.firstChild)
				comment.appendChild (srvices.newIframe ());
			if (comment.firstChild.src.indexOf ("addcomment") < 0)
				comment.firstChild.src = urlComments;

		}*/
	},

	srvReplyCommentHide: function (srv, id_comentario, bp) {
		if (srv.className.indexOf ("sunk") == -1)
			return false;
		var comment = document.getElementById (id_comentario);
		comment.style.visibility = "hidden";
		comment.firstChild.style.visibility = "hidden";
		comment.removeChild(comment.firstChild);
		comment.appendChild (srvices.newIframe());
		comment.firstChild.className = 'reply';
		/*SHARETHIS.showEmbeds ();*/ 
                if (bp == 'rb') {
			srv.className = "button reply";
        	        srv.firstChild.nodeValue = "Responder a este comentario";
		} else {
			srv.className = "button addcomment";
        	        srv.firstChild.nodeValue = "Añadir comentario";
		}
		return true;
	},

	srvClickReplyComment: function (srv, id_comentario, bp) {
		var replybutton = bp + id_comentario;
		var srv = document.getElementById (replybutton);
		var trapid = document.getElementById ("renderComments");
                var renderCommentsVal = bp + id_comentario;
                if (trapid.className != 0 && trapid.className != renderCommentsVal) {
			var otherbutton = trapid.className;
			var othersrv = document.getElementById (otherbutton);
			var trapidVal = trapid.className;
			if (trapidVal.match('rb')) {
	                        trapidVal = trapidVal.replace('rb', '');
				cbp = 'rb';
			} else {
	                        trapidVal = trapidVal.replace('cb', '');
				cbp = 'cb';
			}
			srvices.srvReplyCommentHide (othersrv, trapidVal, cbp);
			trapid.className = 0;
		}
                main.footerdShow(true);
		if (!srvices.srvReplyCommentHide (srv, id_comentario, bp)) {
			trapid.className = bp + id_comentario;
                        if (bp == 'rb') {
                        	srv.className = "button reply sunk";
                        	srv.firstChild.nodeValue = "Cerrar";
			} else {
				srv.className = "button addcomment sunk";
                        	srv.firstChild.nodeValue = "Cerrar";
        			}
			var comment = document.getElementById (id_comentario);
			comment.style.display = "block";
			if (bp == 'rb')
				var idcomentariostr = '?id_comentario=' + id_comentario;
			else
				var idcomentariostr = '';
			if (!comment.firstChild)
				comment.appendChild (srvices.newIframe ("reply"));
			if (comment.firstChild.src.indexOf ("addcomment") < 0) {
				comment.firstChild.src =  '/v7.0/cgi/comentarios.php' + idcomentariostr;
	                        comment.firstChild.style.visibility = "visible";


                        }
                        main.footerdShow(false);
                } else
			trapid.className = 0;
	}, 

	srvClickFont: function (evt, srv) {
		var textDiv = document.getElementById ("article-text");
		var font;
		
		switch (dodo.getComputedStyle (textDiv, "font-size")) {
		case "16px": size = "18px"; break;
		case "18px": size = "20px"; break;
		case "20px": size = "22px"; break;
		case "22px": 
			size = "16px";
			switch (textDiv.style.fontFamily.substr (0, 3)) {
			case "Hel":
				font = 'Times, "Times New Roman", serif';
				break;
			case "Tim":
			default:
				font = "Helvetica, Arial, sans-serif";
			}
		}

		textDiv.style.fontSize = size;
		dodo.cookieSet ("textSize", size, 999);
		if (font) {
			textDiv.style.fontFamily = font;
			dodo.cookieSet ("textFont", font, 999);
		}
	}, 

	setCols1: function (srv) {
		var text = document.getElementById ("article-text");
		for (var i = 0, node; node = text.childNodes[i]; i++)
			if (node.className && node.className.indexOf ("col") > -1)
				node.className = "col";
		srv.style.display = "none";
		document.getElementById ("srv-cols2").style.display = "inline";
	}, 

	srvClickCols1: function (evt, srv) {
		srvices.setCols1 (srv);
		dodo.cookieSet ("textCols", "1", 999);
	}, 

	srvClickCols2: function (evt, srv) {
		var text = document.getElementById ("article-text");
		var col = 1;
		for (var i = 0, node; node = text.childNodes[i]; i++)
			if (node.className && node.className.indexOf ("col") > -1)
				node.className = "col col" + col++;
		srv.style.display = "none";
		document.getElementById ("srv-cols1").style.display = "inline";

		dodo.cookieSet ("textCols", "2", 999);
	},

	sharethisTweak: function () {
		var stwrapper = document.getElementById ("stwrapper");
		if (stwrapper) {
			stwrapper.firstChild.onclick = function () {};
			document.getElementById ("srv-share").onclick = srvices.srvClickShare;
		} else
			dodo.delayRun (srvices.sharethisTweak, 200);
	},

	init: function () {
		var srv = document.getElementById ("srv-cols1");
		if (srv) {
			if (!document.getElementById ("srv-cols2")) { // El documento es de sólo una columna.
				srv.className = "button-disable";
				srv.onclick = function () { return false; };
			} else {
				if (dodo.cookieGet ("textCols") == "1")
					srvices.setCols1 (srv);
			}
		}

		srv = document.getElementById ("srv-share");
		if (srv) {
			SHARETHIS.shareables[SHARETHIS.shareables.length - 1].attachButton (srv);
			srvices.sharethisTweak (srv);
		}
	},
        
        hideMainSumariosTitle: function (status) {
	    var mylist=document.getElementById("other-articles");
            if (mylist)
 	      for (i=0; i<mylist.childNodes.length; i++){
		if (mylist.childNodes[i].nodeName=="DIV")
		    if (status) {
			mylist.childNodes[i].style.visibility = 'hidden';
		    } else {
			mylist.childNodes[i].style.visibility = 'visible';
		    }
	     }
        }
}
