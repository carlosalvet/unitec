var dshbrdFrames = [];

window.addEventListener('resize',resizeIframe);
function resizeIframe(){
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function(){
        console.log('Resized finished.');
        frmwkResizeIframes();
    }, 250);
}

function getParentParams( ) {
	queryString = window.location.href;
    queries = queryString.split("?");
    if (queries.length > 1){
		return queries[1];
	} else {
		//No parameters
		return '';
	}
};
function frmwrkSetSource(obj,ssrc){
	//Move this to frmwkResize so that visualizations which shouldn't resize initially don't on browser resize.
	//frmwkRegisterIframe(obj);
	if(!obj.src || obj.src == ''){
		//Need to add the parent URL query parameters to the source so that visualizations get the correct context
		var appender = '?';
		if (ssrc.indexOf('?') > -1){
			appender = '&'
		} 
		var params = getParentParams();
		if (params != ''){
			ssrc += appender + params;
		}
		obj.src = ssrc;
	}
}
function frmwkResize(obj,resizeToParent){
	frmwkRegisterIframe(obj);
	if (resizeToParent){
		obj.style.height = obj.parentElement.offsetHeight + 'px';
	} else {
		//Note: the visualization should have scroll:visible for this to work
		//Use scrollHeight as it doesn't change when CSS is applied
		if ('WebkitAppearance' in document.documentElement.style){
			obj.style.height = obj.contentWindow.document.body.offsetHeight + 20 +'px';
		} else {
			//Use scrollHeight as it doesn't change when CSS is applied
			obj.style.height = obj.contentWindow.document.body.scrollHeight + 20 +'px';
		}

    }
}
function frmwkRegisterIframe(obj){
	if (dshbrdFrames.indexOf(obj.id) < 0){
		//register the iframe
		dshbrdFrames.push(obj.id);
	}
}
function frmwkResizeIframes(){
	for (i = 0; i < dshbrdFrames.length; i++) {
		if (document.getElementById(dshbrdFrames[i])){
			var obj = document.getElementById(dshbrdFrames[i]);
			//Fix for Chrome/Safari
			if ('WebkitAppearance' in document.documentElement.style){
				obj.style.height = obj.contentWindow.document.body.offsetHeight + 20 +'px';
			} else {
				//Use scrollHeight as it doesn't change when CSS is applied
				obj.style.height = obj.contentWindow.document.body.scrollHeight + 20 +'px';
			}
		}
	}
}