'use strict';

var ldbTimeIn = 0;
var ldbTotalTimeOut;
var ldbAssessmentId;
var ldbCourseId; 
var ldbUserId = '';
var ldbTotalExits;
var ldbSumTimeOut;
var ldbCount = 0;
var ldbreportArray = [];
var buttonFinishTest = document.getElementsByClassName('submit button-1')[0];
var buttonFinishTestMultiPage1 = document.getElementsByClassName('submit')[0];
var buttonFinishTestMultiPage2 = document.getElementsByClassName('submit')[1];
var isNotFinished = true;
var lp_assessment_attempt_id ='';

var iconElement = document.querySelectorAll('link[type="image/x-icon"]')[0];
var originalIcon = iconElement.href;
var alertIcon = "/webapps/lnoh-jarvis-BBLEARN/resources/imgs/danger.png"

var titleElement = document.getElementsByTagName('title')[0];
var originalText = titleElement.innerHTML;
var isWindowExamVisible = false;
var wasIframeTextAreaclicked = false;
ldbsetVariables();
var activeElement = document.activeElement;
var wasVivibilityListenerExecuted = false;

var ldbElements = {
    tags: ["IFRAME"],
    states: ["hidden"],
    classes: ["button-4","button-2","hiddenInput"]
};


function ldbTriggerClickEvent(){
  let button = document.getElementById('buttonTrigger');
  button.setAttribute('onclick', " localStorage.setItem('full_screen', true);");
  button.click();

}

function ldbButtonInsideTest(){
  let bodyPage = document.body;
  let buttonTest = document.createElement('button');
  buttonTest.setAttribute('id', 'buttonTest');
  buttonTest.display = 'none';
  bodyPage.insertBefore(buttonTest, bodyPage.childNodes[0]);

  let buttonInsideTest = document.getElementById('buttonTest');
  buttonInsideTest.setAttribute('onclick', "startFullScreen()");
  buttonInsideTest.click();
}


function startLockDownBrowser(){
  ldbSetLpAssessmentAttemptId();

  //Event Listener used to display the out of focus modal alert to handle the use cases when the user opens a new tab or window with keyboard shortcuts
   document.addEventListener("visibilitychange", function() {
    if(ldbElements.states.includes(document.visibilityState) && (ldbElements.tags.includes(activeElement.tagName)||ldbElements.classes.includes(activeElement.className))){
      ldbDisplayModalAlert();
      wasVivibilityListenerExecuted = true;
    }
  });
    

  window.addEventListener("blur", ldbGoOut);
  window.addEventListener("focus", ldbGetTimeInside);
  ldbCreateModalAlert();
  ldbOverwriteBBFunction();
  if(buttonFinishTest!== undefined){
    buttonFinishTest.setAttribute("onclick", "ldbChangeFlag();return assessment.doConfirmSubmit(submitAssessment);")
  }
  if(buttonFinishTestMultiPage1 !== undefined){
    buttonFinishTestMultiPage1.setAttribute("onclick", "ldbChangeFlag();return assessment.doConfirmSubmit(submitAssessment);")
  }
  if(buttonFinishTestMultiPage2 !== undefined){
    buttonFinishTestMultiPage2.setAttribute("onclick", "ldbChangeFlag();return assessment.doConfirmSubmit(submitAssessment);")
  }
}

function ldbChangeFlag(){
  isNotFinished = false;
}

function ldbsetVariables(){
  
  if(localStorage.getItem("total_count_out")=="0" || localStorage.getItem("total_count_out")=="" || localStorage.getItem("total_count_out") === null || localStorage.getItem("total_count_out") == "undefined" || isNaN(localStorage.getItem("total_count_out"))){
    ldbTotalExits = 0;
    localStorage.setItem("total_count_out",ldbTotalExits);
    ldbreportArray = [];
    localStorage.setItem("report_lockdown",JSON.stringify(ldbreportArray));
  }else{
    ldbTotalExits = parseInt(localStorage.getItem("total_count_out"));
    ldbreportArray = JSON.parse(localStorage.getItem("report_lockdown"));
  }

  if(localStorage.getItem("sum_time_out")=="0" || localStorage.getItem("sum_time_out")=="" || localStorage.getItem("sum_time_out") === null || localStorage.getItem("sum_time_out") == "undefined" || isNaN(localStorage.getItem("sum_time_out"))){
    ldbSumTimeOut = 0;
    localStorage.setItem("sum_time_out",ldbSumTimeOut);
    ldbTotalTimeOut = ldbFormatTime(ldbSumTimeOut);
    localStorage.setItem("total_time_out",ldbTotalTimeOut);
  }else{
    ldbSumTimeOut = parseInt(localStorage.getItem("sum_time_out"));
    ldbTotalTimeOut = ldbFormatTime((ldbSumTimeOut/60));
  }
}

function ldbClearStorage(){
  localStorage.setItem("total_count_out","0");
  localStorage.setItem("total_time_out","00:00");
  localStorage.setItem("report_lockdown","[]");
  localStorage.setItem("sum_time_out","0");
  localStorage.setItem("countPic","0");
}
                                                               
function ldbGoOut(){
activeElement = document.activeElement;
//No display the out of focus modal alert, "if" to handle false positives and avoid alerts when the user clicks on a text area, browse my computer and save answers button
if(ldbElements.tags.includes(activeElement.tagName)||ldbElements.classes.includes(activeElement.className) ){
  //Do nothing, no display modal alert
  activeElement.blur();
  if(activeElement.tagName == "IFRAME"){
      activeElement.addEventListener("mouseout", function _listener() {
       window.focus();
      activeElement.removeEventListener("mouseout", _listener, true);
    }, true);
  }

}
//Display the modal alert when the window loses focus
else{
    if(wasVivibilityListenerExecuted != true ){
      ldbDisplayModalAlert();
      }
  }
wasVivibilityListenerExecuted = false;
}

function ldbDisplayModalAlert(){

ldbsetVariables(); 
  ldbTotalExits += 1;
  ldbTimeIn = new Date();
 
  iconElement.href = alertIcon;
  document.getElementsByTagName('head')[0].appendChild(iconElement);
  changeTitleWindow();
 
  ldbGetData("LOST_FOCUS", "--");
 
  let modal = document.getElementById('modalAlert');
  if(modal !== null || modal !== undefined){
    let modalMessage = document.getElementById('countExist');
 
    modalMessage.innerHTML = i18n("EXIST");
    let numberToDelete =  modalMessage.innerHTML.split("#")[1];
    modalMessage.innerHTML = modalMessage.innerHTML.replace(numberToDelete, ldbTotalExits)
    modal.style.display = 'block';
  }
}

function changeTitleWindow(){
    if(current_lang === "es"){
      titleElement.innerHTML = "¡ALERTA!("+ldbTotalExits+")";
    }else if(current_lang === "pt"){
      titleElement.innerHTML = "ALERTA!("+ldbTotalExits+")";
    }else{
      titleElement.innerHTML = "WARNING!("+ldbTotalExits+")";
    }
}

function ldbGetTimeInside(){ 
  ldbsetVariables();
  let time = new Date();
  let totalTime;

  iconElement.href = originalIcon;
  document.getElementsByTagName('head')[0].appendChild(iconElement);

  titleElement.innerHTML = originalText;

  if(ldbTimeIn == 0){
    totalTime = 0

  }else{
     totalTime = time - ldbTimeIn;
  }


  let totalSeconds = Math.round(totalTime / (1000))
  let realTime =  ldbFormatTime((totalSeconds/60));

  ldbSumTimeOut = ldbSumTimeOut + totalSeconds;
  ldbTotalTimeOut = ldbFormatTime((ldbSumTimeOut/60));

 

  ldbGetData("GET_FOCUS", realTime);
  if(!isNotFinished){
    closeModal();
  }
  isNotFinished = true;
  
}

function ldbGetDate(){
  let today = new Date();
  let dd = (today.getDate() < 10) ? "0" + today.getDate() : today.getDate();
  let mm = (today.getMonth() < 10) ? "0" + (today.getMonth()+1) : (today.getMonth()+1); //January 0
  let yyyy = today.getFullYear();
  today = mm + '/' + dd + '/' + yyyy;

  return today;
}

function ldbGetHour(){
  let d = new Date();
  let h = (d.getHours() < 10 ) ? "0" + d.getHours() : d.getHours();
  let m = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
  let s = (d.getSeconds() < 10 ) ? "0" + d.getSeconds() : d.getSeconds();
  let hour = h + ":" + m + ":" + s; 
        
  return hour;
}

function ldbGetData(typeEvent, time){
  if(isNotFinished){
    ldbCount++;

    ldbGetGeneralData();

    var lockdown_browser =  {
          number: ldbCount,
          type_event: typeEvent,
          date: ldbGetDate(),
          hour: ldbGetHour(),
          time_out: time
      }
    
    ldbreportArray.push(lockdown_browser);
    localStorage.setItem("report_lockdown", JSON.stringify(ldbreportArray));
    localStorage.setItem("total_time_out", ldbTotalTimeOut);
    localStorage.setItem("total_count_out", ldbTotalExits);
    localStorage.setItem("sum_time_out",ldbSumTimeOut);
    
  }

}

function ldbFormatTime(minutes){
  let sign = minutes < 0 ? "-" : "";
  let min = Math.floor(Math.abs(minutes));
  let sec = Math.floor((Math.abs(minutes) * 60) % 60);
  let time = sign + (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
  return time;
}

function ldbCreateReport(){
  let dateOffset = new Date().getTimezoneOffset() * 60000;
  
  var report = {
   user_id: localStorage.getItem("user_id"),
   course_id: localStorage.getItem("course_id"),
   assessment_id : localStorage.getItem("assessment_id"),
   lockdown_report : JSON.parse(localStorage.getItem("report_lockdown")),
   total_time_out: localStorage.getItem("total_time_out"),
   total_count_out: localStorage.getItem("total_count_out"),
   timestamp: new Date(Date.now() - dateOffset)
          .toISOString()
          .replace(/\.[0-9]{3}/, ''),
   lp_assessment_attempt_id: localStorage.getItem("lp_assessment_attempt_id")
   
  }

  let data = JSON.stringify(report);
  var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance
  xmlhttp.open("POST", "/webapps/lnoh-jarvis-BBLEARN/saveLockDownReport");
  xmlhttp.setRequestHeader("Content-Type", "application/json");
  xmlhttp.send(data);
  ldbClearStorage();
}

function ldbGetGeneralData(){
 if(ldbUserId !== ''){
  ldbAssessmentId = getURLParameter('course_assessment_id')
  ldbCourseId = getURLParameter('course_id')
  
  localStorage.setItem("user_id", ldbUserId);
  localStorage.setItem("course_id", ldbCourseId);
  localStorage.setItem("assessment_id", ldbAssessmentId);

 }else{
   ldbGetCurrentUserName();
 }
} 

function ldbGetCurrentUserName(){
let url = '/webapps/lnoh-jarvis-BBLEARN/getCurrentUserName'
jQ
  .ajax({
    url: url,
    type: 'GET'
  })
  .done(function (userName) {
    ldbUserId = userName
  })
}


function ldbGetURLParameter(parameterName){
var result = null, tmp = []
var items = location.search.substr(1).split('&')
for (var index = 0; index < items.length; index++) {
  tmp = items[index].split('=')
  if (tmp[0] === parameterName){
    if( decodeURIComponent(tmp[1]) !== undefined ){
     result = decodeURIComponent(tmp[1]);
    }
  }
}
return result
}

function ldbCreateModalAlert(){
  let headTag = document.head

  let link1 = document.createElement('link')
  link1.rel = 'stylesheet'
  link1.href =
    '/webapps/lnoh-jarvis-BBLEARN/resources/css/modal_alert.css'
  headTag.appendChild(link1)

  let ldbBodyPage = document.body;

  let divModal = document.createElement('div');
  divModal.setAttribute('class', 'modal')
  divModal.setAttribute('id', 'modalAlert');
  divModal.style.display = 'none'
  ldbBodyPage.insertBefore(divModal, ldbBodyPage.childNodes[0])

  let divContent = document.getElementById('modalAlert');

  let divModalContent = document.createElement('div');
  divModalContent.setAttribute('id', 'modalAlertContet');
  divModalContent.setAttribute('class', 'modal-content')
  divContent.appendChild(divModalContent);

  let centerTag = document.createElement('center');
  let h2Tag = document.createElement('h2');
  let pTag = document.createElement('p');
  let bTag = document.createElement('b');
  bTag.innerHTML = i18n('WARNING');
  pTag.appendChild(bTag);
  h2Tag.appendChild(pTag);
  centerTag.appendChild(h2Tag);

  let p2Tag = document.createElement('p');
  p2Tag.setAttribute('id', 'countExist');
  p2Tag.innerHTML = i18n('EXIST');
  p2Tag.innerHTML += ldbTotalExits;
  let p3Tag = document.createElement('p');
  p3Tag.innerHTML = i18n('MESSAGE_ONE');

  centerTag.appendChild(p2Tag);
  centerTag.appendChild(p3Tag);

  let imgTag = document.createElement('img');
  imgTag.setAttribute('src', alertIcon);
  imgTag.setAttribute('alt', 'WARNING');
  imgTag.setAttribute('width', '200');
  imgTag.setAttribute('height', '200');

  let p4Tag = document.createElement('p');
  
  p4Tag.innerHTML = i18n('ALERT_ONE');
  let p5Tag = document.createElement('p');
  p5Tag.innerHTML = i18n('ALERT_TWO');
  let p6Tag = document.createElement('p');
  p6Tag.innerHTML = i18n('ALERT_THREE');

  let brTag = document.createElement('br');

  let buttonTag = document.createElement('button');
  buttonTag.setAttribute('id', 'closeModal');
  buttonTag.setAttribute('onclick', 'closeModal()');
  buttonTag.innerHTML = i18n('BUTTON_CLOSE');

  centerTag.appendChild(imgTag);
  centerTag.appendChild(p4Tag);
  centerTag.appendChild(p5Tag);
  centerTag.appendChild(p6Tag);
  centerTag.appendChild(brTag);
  centerTag.appendChild(buttonTag);

  divModalContent.appendChild(centerTag);

} 

function closeModal(){
let modal = document.getElementById('modalAlert');

if(modal !== null || modal !== undefined){
  modal.style.display = 'none'
}
}

function deleteModal(){
jQ("#modalAlert").remove();
}

function ldbOverwriteBBFunction(){
assessment.doConfirmSubmit = function( msg ){
  document.forms.saveAttemptForm.save_and_submit.value = 'true';
  ldbTotalExits--;
  try
  {
    if ( confirmSubmit( msg ) )
    {
      deleteModal();
      assessment.resetFields();
      assessment.submitAttemptForm();
    }
    closeModal();
  }
  catch ( err )
  {
  }
  document.forms.saveAttemptForm.save_and_submit.value = '';
  return false;
};
}

function ldbSetLpAssessmentAttemptId(){
lp_assessment_attempt_id = document.getElementById('current_attempt_item_id');
if(lp_assessment_attempt_id != null){
   var lp_assessment_attempt_id_value = lp_assessment_attempt_id.value;
   localStorage.setItem("lp_assessment_attempt_id", lp_assessment_attempt_id_value);
 }else{
  var random_id = Math.random().toString(36).replace('0.', '').substr(1, 4);
  var lp_assessment_attempt_id_random = 'RV_' + random_id;
  localStorage.setItem("lp_assessment_attempt_id", lp_assessment_attempt_id_random);

 }
}